# FortiManager

Vendor: Fortinet
Homepage: https://www.fortinet.com/

Product: FortiManager
Product Page: https://www.fortinet.com/products/management/fortimanager

## Introduction
We classify FortiManager into the Security/SASE domain as FortiManager provides a firewall and security managment solution. 

"FortiManager simplifies firewall network and security management across on-premises and cloud deployments as part of a hybrid mesh firewall environment"

## Why Integrate
The FortiManager adapter from Itential is used to integrate the Itential Automation Platform (IAP) with FortiManager in order to manage security across environments. With this adapter you have the ability to perform operations on items such as:

- VDOM
- ADOM
- SD-WAN

## Additional Product Documentation
The [Postman Collection for FortiManager](https://www.postman.com/fortinet-emea-tac-api-team/workspace/fortinet/documentation/22422020-f33ce7c0-31d3-42e0-8ea3-717d84719c2d)