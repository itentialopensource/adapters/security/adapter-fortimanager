
## 0.4.0 [05-24-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-fortimanager!6

---