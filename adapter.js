/* @copyright Itential, LLC 2019 (pre-modifications) */

/* eslint import/no-dynamic-require: warn */
/* eslint object-curly-newline: warn */

// Set globals
/* global log */

/* Required libraries.  */
const path = require('path');

/* Fetch in the other needed components for the this Adaptor */
const AdapterBaseCl = require(path.join(__dirname, 'adapterBase.js'));

/**
 * This is the adapter/interface into FortiManager
 */

/* GENERAL ADAPTER FUNCTIONS */
class FortiManager extends AdapterBaseCl {
  /**
   * FortiManager Adapter
   * @constructor
   */
  /* Working on changing the way we do Emit methods due to size and time constrainsts
  constructor(prongid, properties) {
    // Instantiate the AdapterBase super class
    super(prongid, properties);

    const restFunctionNames = this.getWorkflowFunctions();

    // Dynamically bind emit functions
    for (let i = 0; i < restFunctionNames.length; i += 1) {
      // Bind function to have name fnNameEmit for fnName
      const version = restFunctionNames[i].match(/__v[0-9]+/);
      const baseFnName = restFunctionNames[i].replace(/__v[0-9]+/, '');
      const fnNameEmit = version ? `${baseFnName}Emit${version}` : `${baseFnName}Emit`;
      this[fnNameEmit] = function (...args) {
        // extract the callback
        const callback = args[args.length - 1];
        // slice the callback from args so we can insert our own
        const functionArgs = args.slice(0, args.length - 1);
        // create a random name for the listener
        const eventName = `${restFunctionNames[i]}:${Math.random().toString(36)}`;
        // tell the calling class to start listening
        callback({ event: eventName, status: 'received' });
        // store parent for use of this context later
        const parent = this;
        // store emission function
        const func = function (val, err) {
          parent.removeListener(eventName, func);
          parent.emit(eventName, val, err);
        };
        // Use apply to call the function in a specific context
        this[restFunctionNames[i]].apply(this, functionArgs.concat([func])); // eslint-disable-line prefer-spread
      };
    }

    // Uncomment if you have things to add to the constructor like using your own properties.
    // Otherwise the constructor in the adapterBase will be used.
    // Capture my own properties - they need to be defined in propertiesSchema.json
    // if (this.allProps && this.allProps.myownproperty) {
    //   mypropvariable = this.allProps.myownproperty;
    // }
  }
  */

  /**
   * @callback healthCallback
   * @param {Object} reqObj - the request to send into the healthcheck
   * @param {Callback} callback - The results of the call
   */
  healthCheck(reqObj, callback) {
    // you can modify what is passed into the healthcheck by changing things in the newReq
    let newReq = null;
    if (reqObj) {
      newReq = Object.assign(...reqObj);
    }
    super.healthCheck(newReq, callback);
  }

  /**
   * @iapGetAdapterWorkflowFunctions
   */
  iapGetAdapterWorkflowFunctions(inIgnore) {
    let myIgnore = [
      'healthCheck',
      'iapGetAdapterWorkflowFunctions',
      'hasEntities',
      'getAuthorization'
    ];
    if (!inIgnore && Array.isArray(inIgnore)) {
      myIgnore = inIgnore;
    } else if (!inIgnore && typeof inIgnore === 'string') {
      myIgnore = [inIgnore];
    }

    // The generic adapter functions should already be ignored (e.g. healthCheck)
    // you can add specific methods that you do not want to be workflow functions to ignore like below
    // myIgnore.push('myMethodNotInWorkflow');

    return super.iapGetAdapterWorkflowFunctions(myIgnore);
  }

  /**
   * iapUpdateAdapterConfiguration is used to update any of the adapter configuration files. This
   * allows customers to make changes to adapter configuration without having to be on the
   * file system.
   *
   * @function iapUpdateAdapterConfiguration
   * @param {string} configFile - the name of the file being updated (required)
   * @param {Object} changes - an object containing all of the changes = formatted like the configuration file (required)
   * @param {string} entity - the entity to be changed, if an action, schema or mock data file (optional)
   * @param {string} type - the type of entity file to change, (action, schema, mock) (optional)
   * @param {string} action - the action to be changed, if an action, schema or mock data file (optional)
   * @param {boolean} replace - true to replace entire mock data, false to merge/append
   * @param {Callback} callback - The results of the call
   */
  iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, replace, callback) {
    const meth = 'adapter-iapUpdateAdapterConfiguration';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    super.iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, replace, callback);
  }

  /**
    * @summary Suspends adapter
    *
    * @function iapSuspendAdapter
    * @param {Callback} callback - callback function
    */
  iapSuspendAdapter(mode, callback) {
    const meth = 'adapter-iapSuspendAdapter';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapSuspendAdapter(mode, callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
    * @summary Unsuspends adapter
    *
    * @function iapUnsuspendAdapter
    * @param {Callback} callback - callback function
    */
  iapUnsuspendAdapter(callback) {
    const meth = 'adapter-iapUnsuspendAdapter';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapUnsuspendAdapter(callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
    * @summary Get the Adapter Queue
    *
    * @function iapGetAdapterQueue
    * @param {Callback} callback - callback function
    */
  iapGetAdapterQueue(callback) {
    const meth = 'adapter-iapGetAdapterQueue';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    return super.iapGetAdapterQueue(callback);
  }

  /* SCRIPT CALLS */
  /**
   * See if the API path provided is found in this adapter
   *
   * @function iapFindAdapterPath
   * @param {string} apiPath - the api path to check on
   * @param {Callback} callback - The results of the call
   */
  iapFindAdapterPath(apiPath, callback) {
    const meth = 'adapter-iapFindAdapterPath';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    super.iapFindAdapterPath(apiPath, callback);
  }

  /**
  * @summary Runs troubleshoot scripts for adapter
  *
  * @function iapTroubleshootAdapter
  * @param {Object} props - the connection, healthcheck and authentication properties
  *
  * @param {boolean} persistFlag - whether the adapter properties should be updated
  * @param {Callback} callback - The results of the call
  */
  iapTroubleshootAdapter(props, persistFlag, callback) {
    const meth = 'adapter-iapTroubleshootAdapter';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapTroubleshootAdapter(props, persistFlag, this, callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
    * @summary runs healthcheck script for adapter
    *
    * @function iapRunAdapterHealthcheck
    * @param {Adapter} adapter - adapter instance to troubleshoot
    * @param {Callback} callback - callback function
    */
  iapRunAdapterHealthcheck(callback) {
    const meth = 'adapter-iapRunAdapterHealthcheck';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapRunAdapterHealthcheck(this, callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
    * @summary runs connectivity check script for adapter
    *
    * @function iapRunAdapterConnectivity
    * @param {Callback} callback - callback function
    */
  iapRunAdapterConnectivity(callback) {
    const meth = 'adapter-iapRunAdapterConnectivity';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapRunAdapterConnectivity(callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
    * @summary runs basicGet script for adapter
    *
    * @function iapRunAdapterBasicGet
    * @param {Callback} callback - callback function
    */
  iapRunAdapterBasicGet(callback) {
    const meth = 'adapter-iapRunAdapterBasicGet';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapRunAdapterBasicGet(callback);
    } catch (error) {
      log.error(`${origin}: ${error}`);
      return callback(null, error);
    }
  }

  /**
   * @summary moves entites into Mongo DB
   *
   * @function iapMoveAdapterEntitiesToDB
   * @param {getCallback} callback - a callback function to return the result (Generics)
   *                                  or the error
   */
  iapMoveAdapterEntitiesToDB(callback) {
    const meth = 'adapter-iapMoveAdapterEntitiesToDB';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapMoveAdapterEntitiesToDB(callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Deactivate adapter tasks
   *
   * @function iapDeactivateTasks
   *
   * @param {Array} tasks - List of tasks to deactivate
   * @param {Callback} callback
   */
  iapDeactivateTasks(tasks, callback) {
    const meth = 'adapter-iapDeactivateTasks';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapDeactivateTasks(tasks, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Activate adapter tasks that have previously been deactivated
   *
   * @function iapActivateTasks
   *
   * @param {Array} tasks - List of tasks to activate
   * @param {Callback} callback
   */
  iapActivateTasks(tasks, callback) {
    const meth = 'adapter-iapActivateTasks';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapActivateTasks(tasks, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /* CACHE CALLS */
  /**
   * @summary Populate the cache for the given entities
   *
   * @function iapPopulateEntityCache
   * @param {String/Array of Strings} entityType - the entity type(s) to populate
   * @param {Callback} callback - whether the cache was updated or not for each entity type
   *
   * @returns status of the populate
   */
  iapPopulateEntityCache(entityTypes, callback) {
    const meth = 'adapter-iapPopulateEntityCache';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapPopulateEntityCache(entityTypes, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Retrieves data from cache for specified entity type
   *
   * @function iapRetrieveEntitiesCache
   * @param {String} entityType - entity of which to retrieve
   * @param {Object} options - settings of which data to return and how to return it
   * @param {Callback} callback - the data if it was retrieved
   */
  iapRetrieveEntitiesCache(entityType, options, callback) {
    const meth = 'adapter-iapCheckEiapRetrieveEntitiesCachentityCached';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapRetrieveEntitiesCache(entityType, options, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /* BROKER CALLS */
  /**
   * @summary Determines if this adapter supports any in a list of entities
   *
   * @function hasEntities
   * @param {String} entityType - the entity type to check for
   * @param {Array} entityList - the list of entities we are looking for
   *
   * @param {Callback} callback - A map where the entity is the key and the
   *                              value is true or false
   */
  hasEntities(entityType, entityList, callback) {
    const meth = 'adapter-hasEntities';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.hasEntities(entityType, entityList, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Get Appliance that match the deviceName
   *
   * @function getDevice
   * @param {String} deviceName - the deviceName to find (required)
   *
   * @param {getCallback} callback - a callback function to return the result
   *                                 (appliance) or the error
   */
  getDevice(deviceName, callback) {
    const meth = 'adapter-getDevice';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.getDevice(deviceName, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Get Appliances that match the filter
   *
   * @function getDevicesFiltered
   * @param {Object} options - the data to use to filter the appliances (optional)
   *
   * @param {getCallback} callback - a callback function to return the result
   *                                 (appliances) or the error
   */
  getDevicesFiltered(options, callback) {
    const meth = 'adapter-getDevicesFiltered';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.getDevicesFiltered(options, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Gets the status for the provided appliance
   *
   * @function isAlive
   * @param {String} deviceName - the deviceName of the appliance. (required)
   *
   * @param {configCallback} callback - callback function to return the result
   *                                    (appliance isAlive) or the error
   */
  isAlive(deviceName, callback) {
    const meth = 'adapter-isAlive';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.isAlive(deviceName, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Gets a config for the provided Appliance
   *
   * @function getConfig
   * @param {String} deviceName - the deviceName of the appliance. (required)
   * @param {String} format - the desired format of the config. (optional)
   *
   * @param {configCallback} callback - callback function to return the result
   *                                    (appliance config) or the error
   */
  getConfig(deviceName, format, callback) {
    const meth = 'adapter-getConfig';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.getConfig(deviceName, format, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * @summary Gets the device count from the system
   *
   * @function iapGetDeviceCount
   *
   * @param {getCallback} callback - callback function to return the result
   *                                    (count) or the error
   */
  iapGetDeviceCount(callback) {
    const meth = 'adapter-iapGetDeviceCount';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapGetDeviceCount(callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /* GENERIC ADAPTER REQUEST - allows extension of adapter without new calls being added */
  /**
   * Makes the requested generic call
   *
   * @function iapExpandedGenericAdapterRequest
   * @param {Object} metadata - metadata for the call (optional).
   *                 Can be a stringified Object.
   * @param {String} uriPath - the path of the api call - do not include the host, port, base path or version (optional)
   * @param {String} restMethod - the rest method (GET, POST, PUT, PATCH, DELETE) (optional)
   * @param {Object} pathVars - the parameters to be put within the url path (optional).
   *                 Can be a stringified Object.
   * @param {Object} queryData - the parameters to be put on the url (optional).
   *                 Can be a stringified Object.
   * @param {Object} requestBody - the body to add to the request (optional).
   *                 Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                 Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result (Generics)
   *                 or the error
   */
  iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback) {
    const meth = 'adapter-iapExpandedGenericAdapterRequest';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * Makes the requested generic call
   *
   * @function genericAdapterRequest
   * @param {String} uriPath - the path of the api call - do not include the host, port, base path or version (required)
   * @param {String} restMethod - the rest method (GET, POST, PUT, PATCH, DELETE) (required)
   * @param {Object} queryData - the parameters to be put on the url (optional).
   *                 Can be a stringified Object.
   * @param {Object} requestBody - the body to add to the request (optional).
   *                 Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                 Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result (Generics)
   *                 or the error
   */
  genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback) {
    const meth = 'adapter-genericAdapterRequest';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /**
   * Makes the requested generic call with no base path or version
   *
   * @function genericAdapterRequestNoBasePath
   * @param {String} uriPath - the path of the api call - do not include the host, port, base path or version (required)
   * @param {String} restMethod - the rest method (GET, POST, PUT, PATCH, DELETE) (required)
   * @param {Object} queryData - the parameters to be put on the url (optional).
   *                 Can be a stringified Object.
   * @param {Object} requestBody - the body to add to the request (optional).
   *                 Can be a stringified Object.
   * @param {Object} addlHeaders - additional headers to be put on the call (optional).
   *                 Can be a stringified Object.
   * @param {getCallback} callback - a callback function to return the result (Generics)
   *                 or the error
   */
  genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback) {
    const meth = 'adapter-genericAdapterRequestNoBasePath';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      return super.genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback);
    } catch (err) {
      log.error(`${origin}: ${err}`);
      return callback(null, err);
    }
  }

  /* INVENTORY CALLS */
  /**
   * @summary run the adapter lint script to return the results.
   *
   * @function iapRunAdapterLint
   * @param {Callback} callback - callback function
   */
  iapRunAdapterLint(callback) {
    const meth = 'adapter-iapRunAdapterLint';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    return super.iapRunAdapterLint(callback);
  }

  /**
   * @summary run the adapter test scripts (baseunit and unit) to return the results.
   *    can not run integration as there can be implications with that.
   *
   * @function iapRunAdapterTests
   * @param {Callback} callback - callback function
   */
  iapRunAdapterTests(callback) {
    const meth = 'adapter-iapRunAdapterTests';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    return super.iapRunAdapterTests(callback);
  }

  /**
   * @summary provide inventory information abbout the adapter
   *
   * @function iapGetAdapterInventory
   * @param {Callback} callback - callback function
   */
  iapGetAdapterInventory(callback) {
    const meth = 'adapter-iapGetAdapterInventory';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    return super.iapGetAdapterInventory(callback);
  }

  /**
   * @callback healthCallback
   * @param {Object} result - the result of the get request (contains an id and a status)
   */
  /**
   * @callback getCallback
   * @param {Object} result - the result of the get request (entity/ies)
   * @param {String} error - any error that occurred
   */
  /**
   * @callback createCallback
   * @param {Object} item - the newly created entity
   * @param {String} error - any error that occurred
   */
  /**
   * @callback updateCallback
   * @param {String} status - the status of the update action
   * @param {String} error - any error that occurred
   */
  /**
   * @callback deleteCallback
   * @param {String} status - the status of the delete action
   * @param {String} error - any error that occurred
   */

  /**
   * @summary This is a generic json rpc api request
   *
   * @function postJsonrpc
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  postJsonrpc(body, callback) {
    const meth = 'adapter-postJsonrpc';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (body === undefined || body === null || body === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['body'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU SET THE DATA TO PASS INTO REQUEST */
    const queryParamsAvailable = {};
    const queryParams = {};
    const pathVars = [];
    const bodyVars = body;

    // loop in template. long callback arg name to avoid identifier conflicts
    Object.keys(queryParamsAvailable).forEach((thisKeyInQueryParamsAvailable) => {
      if (queryParamsAvailable[thisKeyInQueryParamsAvailable] !== undefined && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== null
        && queryParamsAvailable[thisKeyInQueryParamsAvailable] !== '') {
        queryParams[thisKeyInQueryParamsAvailable] = queryParamsAvailable[thisKeyInQueryParamsAvailable];
      }
    });

    // set up the request object - payload, uriPathVars, uriQuery, uriOptions, addlHeaders, authData, callProperties, filter, priority, event
    // see adapter code documentation for more information on the request object's fields
    const reqObj = {
      payload: bodyVars,
      uriPathVars: pathVars,
      uriQuery: queryParams
    };

    try {
      // Make the call -
      // identifyRequest(entity, action, requestObj, returnDataFlag, callback)
      return this.requestHandlerInst.identifyRequest('GenericRequest', 'postJsonrpc', reqObj, true, (irReturnData, irReturnError) => {
        // if we received an error or their is no response on the results
        // return an error
        if (irReturnError) {
          /* HERE IS WHERE YOU CAN ALTER THE ERROR MESSAGE */
          return callback(null, irReturnError);
        }
        if (!Object.hasOwnProperty.call(irReturnData, 'response')) {
          const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Invalid Response', ['postJsonrpc'], null, null, null);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        /* HERE IS WHERE YOU CAN ALTER THE RETURN DATA */
        // return the response
        return callback(irReturnData, null);
      });
    } catch (ex) {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @summary This is a call to get ADOMs
   *
   * @function getAdoms
   * @param {string} adom - the specific adom to retrieve (option)
   * @param {string} expandMember - attributes of members to return (option)
   * @param {array} fields - limit the output by returning only the attributes specified in the string array (optional)
   * @param {array} filter - filter the result according to a set of criteria (optional)
   * @param {number} loadSub - sub objects to return (optional)
   * @param {array} metaFields - meta fields to return (optional)
   * @param {string} option - set fetch option for the request (optional)
   * @param {array} range - limit the number of output (optional)
   * @param {array} sortings - sorting of the returned result (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getAdoms(adom, expandMember, fields, filter, loadSub, metaFields, option, range, sortings, callback) {
    const meth = 'adapter-getAdoms';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU DEFINE VARIABLES */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: '/dvmdb/adom'
        }
      ]
    };
    if (adom !== undefined && adom !== null && adom !== '') {
      body.params[0].url = `/dvmdb/adom/${adom}`;
    }
    if (expandMember !== undefined && expandMember !== null && expandMember !== '') {
      body.params['expand member'] = expandMember;
    }
    if (fields !== undefined && fields !== null && fields !== '') {
      if (Array.isArray(fields)) {
        body.params[0].fields = fields;
      } else if (typeof fields === 'string') {
        body.params[0].fields = [fields];
      }
    }
    if (filter !== undefined && filter !== null && filter !== '') {
      if (Array.isArray(filter)) {
        body.params[0].filter = filter;
      } else if (typeof filter === 'string') {
        body.params[0].filter = [filter];
      }
    }
    if (loadSub !== undefined && loadSub !== null) {
      body.params[0].loadSub = loadSub;
    }
    if (metaFields !== undefined && metaFields !== null && metaFields !== '') {
      if (Array.isArray(metaFields)) {
        body.params['meta fields'] = metaFields;
      } else if (typeof metaFields === 'string') {
        body.params['meta fields'] = [metaFields];
      }
    }
    if (option !== undefined && option !== null && option !== '') {
      body.params[0].option = option;
    }
    if (range !== undefined && range !== null && range !== '') {
      if (Array.isArray(range)) {
        body.params[0].range = range;
      } else if (typeof range === 'string') {
        body.params[0].range = [range];
      }
    }
    if (sortings !== undefined && sortings !== null && sortings !== '') {
      if (Array.isArray(sortings)) {
        body.params[0].sortings = sortings;
      } else if (typeof sortings === 'string') {
        body.params[0].sortings = [sortings];
      }
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create ADOMs
   *
   * @function createAdoms
   * @param {array} adomInfo - the adom information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createAdoms(adomInfo, callback) {
    const meth = 'adapter-createAdoms';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomInfo === undefined || adomInfo === null || adomInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'add',
      params: [
        {
          data: adomInfo,
          url: '/dvmdb/adom'
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to set ADOMs
   *
   * @function setAdoms
   * @param {string} adom - the specific vdom to set (option)
   * @param {array} adomInfo - the adom information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setAdoms(adom, adomInfo, callback) {
    const meth = 'adapter-setAdoms';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomInfo === undefined || adomInfo === null || adomInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'set',
      params: [
        {
          data: adomInfo,
          url: '/dvmdb/adom'
        }
      ]
    };
    if (adom !== undefined && adom !== null && adom !== '') {
      body.params[0].url = `/dvmdb/adom/${adom}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to update ADOMs
   *
   * @function updateAdoms
   * @param {string} adom - the specific adom to update (optional)
   * @param {array} adomInfo - the adom information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateAdoms(adom, adomInfo, callback) {
    const meth = 'adapter-updateAdoms';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomInfo === undefined || adomInfo === null || adomInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'update',
      params: [
        {
          data: adomInfo,
          url: '/dvmdb/adom'
        }
      ]
    };
    if (adom !== undefined && adom !== null && adom !== '') {
      body.params[0].url = `/dvmdb/adom/${adom}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to delete ADOM
   *
   * @function deleteAdom
   * @param {string} adom - the specific adom to delete (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteAdom(adom, callback) {
    const meth = 'adapter-deleteAdom';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adom === undefined || adom === null || adom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'delete',
      params: [
        {
          url: `/dvmdb/adom/${adom}`
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to get devices
   *
   * @function getDevices
   * @param {string} device - the specific device to retrieve (option)
   * @param {string} expandMember - attributes of members to return (option)
   * @param {array} fields - limit the output by returning only the attributes specified in the string array (optional)
   * @param {array} filter - filter the result according to a set of criteria (optional)
   * @param {number} loadSub - sub objects to return (optional)
   * @param {array} metaFields - meta fields to return (optional)
   * @param {string} option - set fetch option for the request (optional)
   * @param {array} range - limit the number of output (optional)
   * @param {array} sortings - sorting of the returned result (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getDevices(device, expandMember, fields, filter, loadSub, metaFields, option, range, sortings, callback) {
    const meth = 'adapter-getDevices';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU DEFINE VARIABLES */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: '/dvmdb/device'
        }
      ]
    };
    if (device !== undefined && device !== null && device !== '') {
      body.params[0].url = `/dvmdb/device/${device}`;
    }
    if (expandMember !== undefined && expandMember !== null && expandMember !== '') {
      body.params['expand member'] = expandMember;
    }
    if (fields !== undefined && fields !== null && fields !== '') {
      if (Array.isArray(fields)) {
        body.params[0].fields = fields;
      } else if (typeof fields === 'string') {
        body.params[0].fields = [fields];
      }
    }
    if (filter !== undefined && filter !== null && filter !== '') {
      if (Array.isArray(filter)) {
        body.params[0].filter = filter;
      } else if (typeof filter === 'string') {
        body.params[0].filter = [filter];
      }
    }
    if (loadSub !== undefined && loadSub !== null) {
      body.params[0].loadSub = loadSub;
    }
    if (metaFields !== undefined && metaFields !== null && metaFields !== '') {
      if (Array.isArray(metaFields)) {
        body.params['meta fields'] = metaFields;
      } else if (typeof metaFields === 'string') {
        body.params['meta fields'] = [metaFields];
      }
    }
    if (option !== undefined && option !== null && option !== '') {
      body.params[0].option = option;
    }
    if (range !== undefined && range !== null && range !== '') {
      if (Array.isArray(range)) {
        body.params[0].range = range;
      } else if (typeof range === 'string') {
        body.params[0].range = [range];
      }
    }
    if (sortings !== undefined && sortings !== null && sortings !== '') {
      if (Array.isArray(sortings)) {
        body.params[0].sortings = sortings;
      } else if (typeof sortings === 'string') {
        body.params[0].sortings = [sortings];
      }
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create Device
   *
   * @function createDevice
   * @param {array} deviceInfo - the device information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createDevice(deviceInfo, callback) {
    const meth = 'adapter-createDevice';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (deviceInfo === undefined || deviceInfo === null || deviceInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['deviceInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'exec',
      params: [
        {
          data: deviceInfo,
          url: 'dvm/cmd/add/device'
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to set devices
   *
   * @function setDevices
   * @param {string} device - the specific device to set (optional)
   * @param {array} deviceInfo - the device information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setDevices(device, deviceInfo, callback) {
    const meth = 'adapter-setDevices';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (deviceInfo === undefined || deviceInfo === null || deviceInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['deviceInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'set',
      params: [
        {
          data: deviceInfo,
          url: '/dvmdb/device'
        }
      ]
    };
    if (device !== undefined && device !== null && device !== '') {
      body.params[0].url = `/dvmdb/device/${device}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to update devices
   *
   * @function updateDevices
   * @param {string} device - the specific device to update (optional)
   * @param {array} deviceInfo - the device information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateDevices(device, deviceInfo, callback) {
    const meth = 'adapter-updateDevices';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (deviceInfo === undefined || deviceInfo === null || deviceInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['deviceInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'update',
      params: [
        {
          data: deviceInfo,
          url: '/dvmdb/device'
        }
      ]
    };
    if (device !== undefined && device !== null && device !== '') {
      body.params[0].url = `/dvmdb/device/${device}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to get device VDOMs
   *
   * @function getDeviceVdoms
   * @param {string} device - the device to retrieve vdoms for (required)
   * @param {string} vdom - the specific vdom to retrieve (option)
   * @param {string} expandMember - attributes of members to return (option)
   * @param {array} fields - limit the output by returning only the attributes specified in the string array (optional)
   * @param {array} filter - filter the result according to a set of criteria (optional)
   * @param {number} loadSub - sub objects to return (optional)
   * @param {string} option - set fetch option for the request (optional)
   * @param {array} range - limit the number of output (optional)
   * @param {array} sortings - sorting of the returned result (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getDeviceVdoms(device, vdom, expandMember, fields, filter, loadSub, option, range, sortings, callback) {
    const meth = 'adapter-getDeviceVdoms';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU DEFINE VARIABLES */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: `/dvmdb/device/${device}/vdom`
        }
      ]
    };
    if (vdom !== undefined && vdom !== null && vdom !== '') {
      body.params[0].url = `/dvmdb/device/${device}/vdom/${vdom}`;
    }
    if (expandMember !== undefined && expandMember !== null && expandMember !== '') {
      body.params['expand member'] = expandMember;
    }
    if (fields !== undefined && fields !== null && fields !== '') {
      if (Array.isArray(fields)) {
        body.params[0].fields = fields;
      } else if (typeof fields === 'string') {
        body.params[0].fields = [fields];
      }
    }
    if (filter !== undefined && filter !== null && filter !== '') {
      if (Array.isArray(filter)) {
        body.params[0].filter = filter;
      } else if (typeof filter === 'string') {
        body.params[0].filter = [filter];
      }
    }
    if (loadSub !== undefined && loadSub !== null) {
      body.params[0].loadSub = loadSub;
    }
    if (option !== undefined && option !== null && option !== '') {
      body.params[0].option = option;
    }
    if (range !== undefined && range !== null && range !== '') {
      if (Array.isArray(range)) {
        body.params[0].range = range;
      } else if (typeof range === 'string') {
        body.params[0].range = [range];
      }
    }
    if (sortings !== undefined && sortings !== null && sortings !== '') {
      if (Array.isArray(sortings)) {
        body.params[0].sortings = sortings;
      } else if (typeof sortings === 'string') {
        body.params[0].sortings = [sortings];
      }
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create devices VDOMs
   *
   * @function createDeviceVdoms
   * @param {string} device - the device to create vdoms on (required)
   * @param {array} vdomInfo - the vdom information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createDeviceVdoms(device, vdomInfo, callback) {
    const meth = 'adapter-createDeviceVdoms';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (vdomInfo === undefined || vdomInfo === null || vdomInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['vdomInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'add',
      params: [
        {
          data: vdomInfo,
          url: `/dvmdb/device/${device}/vdom`
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to set devices VDOMs
   *
   * @function setDeviceVdoms
   * @param {string} device - the device to set vdoms on (required)
   * @param {string} vdom - the specific vdom to set (option)
   * @param {array} vdomInfo - the vdom information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setDeviceVdoms(device, vdom, vdomInfo, callback) {
    const meth = 'adapter-setDeviceVdoms';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (vdomInfo === undefined || vdomInfo === null || vdomInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['vdomInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'set',
      params: [
        {
          data: vdomInfo,
          url: `/dvmdb/device/${device}/vdom`
        }
      ]
    };
    if (vdom !== undefined && vdom !== null && vdom !== '') {
      body.params[0].url = `/dvmdb/device/${device}/vdom/${vdom}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to update device VDOMs
   *
   * @function updateDeviceVdoms
   * @param {string} device - the device to update vdoms on (required)
   * @param {string} vdom - the specific vdom to update (option)
   * @param {array} vdomInfo - the vdom information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateDeviceVdoms(device, vdom, vdomInfo, callback) {
    const meth = 'adapter-updateDeviceVdoms';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (vdomInfo === undefined || vdomInfo === null || vdomInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['vdomInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'update',
      params: [
        {
          data: vdomInfo,
          url: `/dvmdb/device/${device}/vdom`
        }
      ]
    };
    if (device !== undefined && device !== null && device !== '') {
      body.params[0].url = `/dvmdb/device/${device}/vdom/${vdom}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to delete device VDOM
   *
   * @function deleteDeviceVdom
   * @param {string} device - the device to delete vdoms on (required)
   * @param {string} vdom - the specific vdom to delete (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteDeviceVdom(device, vdom, callback) {
    const meth = 'adapter-deleteDeviceVdom';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (vdom === undefined || vdom === null || vdom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['vdom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'delete',
      params: [
        {
          url: `/dvmdb/device/${device}/vdom/${vdom}`
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to get ADOM devices
   *
   * @function getAdomDevices
   * @param {string} adom - the adom to add the device to (required)
   * @param {string} device - the specific device to retrieve (option)
   * @param {string} expandMember - attributes of members to return (option)
   * @param {array} fields - limit the output by returning only the attributes specified in the string array (optional)
   * @param {array} filter - filter the result according to a set of criteria (optional)
   * @param {number} loadSub - sub objects to return (optional)
   * @param {array} metaFields - meta fields to return (optional)
   * @param {string} option - set fetch option for the request (optional)
   * @param {array} range - limit the number of output (optional)
   * @param {array} sortings - sorting of the returned result (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getAdomDevices(adom, device, expandMember, fields, filter, loadSub, metaFields, option, range, sortings, callback) {
    const meth = 'adapter-getAdomDevices';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adom === undefined || adom === null || adom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU DEFINE VARIABLES */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: `/dvmdb/adom/${adom}/device`
        }
      ]
    };
    if (device !== undefined && device !== null && device !== '') {
      body.params[0].url = `/dvmdb/adom/${adom}/device/${device}`;
    }
    if (expandMember !== undefined && expandMember !== null && expandMember !== '') {
      body.params['expand member'] = expandMember;
    }
    if (fields !== undefined && fields !== null && fields !== '') {
      if (Array.isArray(fields)) {
        body.params[0].fields = fields;
      } else if (typeof fields === 'string') {
        body.params[0].fields = [fields];
      }
    }
    if (filter !== undefined && filter !== null && filter !== '') {
      if (Array.isArray(filter)) {
        body.params[0].filter = filter;
      } else if (typeof filter === 'string') {
        body.params[0].filter = [filter];
      }
    }
    if (loadSub !== undefined && loadSub !== null) {
      body.params[0].loadSub = loadSub;
    }
    if (metaFields !== undefined && metaFields !== null && metaFields !== '') {
      if (Array.isArray(metaFields)) {
        body.params['meta fields'] = metaFields;
      } else if (typeof metaFields === 'string') {
        body.params['meta fields'] = [metaFields];
      }
    }
    if (option !== undefined && option !== null && option !== '') {
      body.params[0].option = option;
    }
    if (range !== undefined && range !== null && range !== '') {
      if (Array.isArray(range)) {
        body.params[0].range = range;
      } else if (typeof range === 'string') {
        body.params[0].range = [range];
      }
    }
    if (sortings !== undefined && sortings !== null && sortings !== '') {
      if (Array.isArray(sortings)) {
        body.params[0].sortings = sortings;
      } else if (typeof sortings === 'string') {
        body.params[0].sortings = [sortings];
      }
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to set ADOM devices
   *
   * @function setAdomDevices
   * @param {string} adom - the adom to set the device on (required)
   * @param {string} device - the specific device to set (optional)
   * @param {array} deviceInfo - the device information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setAdomDevices(adom, device, deviceInfo, callback) {
    const meth = 'adapter-setAdomDevices';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adom === undefined || adom === null || adom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (deviceInfo === undefined || deviceInfo === null || deviceInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['deviceInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'set',
      params: [
        {
          data: deviceInfo,
          url: `/dvmdb/adom/${adom}/device`
        }
      ]
    };
    if (device !== undefined && device !== null && device !== '') {
      body.params[0].url = `/dvmdb/adom/${adom}/device/${device}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to update ADOM devices
   *
   * @function updateAdomDevices
   * @param {string} adom - the adom to update the device on (required)
   * @param {string} device - the specific device to update (optional)
   * @param {array} deviceInfo - the device information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateAdomDevices(adom, device, deviceInfo, callback) {
    const meth = 'adapter-updateAdomDevices';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adom === undefined || adom === null || adom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (deviceInfo === undefined || deviceInfo === null || deviceInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['deviceInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'update',
      params: [
        {
          data: deviceInfo,
          url: `/dvmdb/adom/${adom}/device`
        }
      ]
    };
    if (device !== undefined && device !== null && device !== '') {
      body.params[0].url = `/dvmdb/adom/${adom}/device/${device}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to get ADOM device VDOMs
   *
   * @function getAdomDeviceVdoms
   * @param {string} adom - the adom to add the device to (required)
   * @param {string} device - the device to retrieve vdoms for (required)
   * @param {string} vdom - the specific vdom to retrieve (option)
   * @param {string} expandMember - attributes of members to return (option)
   * @param {array} fields - limit the output by returning only the attributes specified in the string array (optional)
   * @param {array} filter - filter the result according to a set of criteria (optional)
   * @param {number} loadSub - sub objects to return (optional)
   * @param {string} option - set fetch option for the request (optional)
   * @param {array} range - limit the number of output (optional)
   * @param {array} sortings - sorting of the returned result (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getAdomDeviceVdoms(adom, device, vdom, expandMember, fields, filter, loadSub, option, range, sortings, callback) {
    const meth = 'adapter-getAdomDeviceVdoms';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adom === undefined || adom === null || adom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU DEFINE VARIABLES */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: `/dvmdb/adom/${adom}/device/${device}/vdom`
        }
      ]
    };
    if (vdom !== undefined && vdom !== null && vdom !== '') {
      body.params[0].url = `/dvmdb/adom/${adom}/device/${device}/vdom/${vdom}`;
    }
    if (expandMember !== undefined && expandMember !== null && expandMember !== '') {
      body.params['expand member'] = expandMember;
    }
    if (fields !== undefined && fields !== null && fields !== '') {
      if (Array.isArray(fields)) {
        body.params[0].fields = fields;
      } else if (typeof fields === 'string') {
        body.params[0].fields = [fields];
      }
    }
    if (filter !== undefined && filter !== null && filter !== '') {
      if (Array.isArray(filter)) {
        body.params[0].filter = filter;
      } else if (typeof filter === 'string') {
        body.params[0].filter = [filter];
      }
    }
    if (loadSub !== undefined && loadSub !== null) {
      body.params[0].loadSub = loadSub;
    }
    if (option !== undefined && option !== null && option !== '') {
      body.params[0].option = option;
    }
    if (range !== undefined && range !== null && range !== '') {
      if (Array.isArray(range)) {
        body.params[0].range = range;
      } else if (typeof range === 'string') {
        body.params[0].range = [range];
      }
    }
    if (sortings !== undefined && sortings !== null && sortings !== '') {
      if (Array.isArray(sortings)) {
        body.params[0].sortings = sortings;
      } else if (typeof sortings === 'string') {
        body.params[0].sortings = [sortings];
      }
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create ADOM devices VDOMs
   *
   * @function createAdomDeviceVdoms
   * @param {string} adom - the adom to add the device to (required)
   * @param {string} device - the device to create vdoms on (required)
   * @param {array} vdomInfo - the vdom information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createAdomDeviceVdoms(adom, device, vdomInfo, callback) {
    const meth = 'adapter-createAdomDeviceVdoms';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adom === undefined || adom === null || adom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (vdomInfo === undefined || vdomInfo === null || vdomInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['vdomInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'add',
      params: [
        {
          data: vdomInfo,
          url: `/dvmdb/adom/${adom}/device/${device}/vdom`
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to set ADOM devices VDOMs
   *
   * @function setAdomDeviceVdoms
   * @param {string} adom - the adom to add the device to (required)
   * @param {string} device - the device to set vdoms on (required)
   * @param {string} vdom - the specific vdom to set (option)
   * @param {array} vdomInfo - the vdom information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setAdomDeviceVdoms(adom, device, vdom, vdomInfo, callback) {
    const meth = 'adapter-setAdomDeviceVdoms';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adom === undefined || adom === null || adom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (vdomInfo === undefined || vdomInfo === null || vdomInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['vdomInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'set',
      params: [
        {
          data: vdomInfo,
          url: `/dvmdb/adom/${adom}/device/${device}/vdom`
        }
      ]
    };
    if (vdom !== undefined && vdom !== null && vdom !== '') {
      body.params[0].url = `/dvmdb/adom/${adom}/device/${device}/vdom/${vdom}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to update ADOM device VDOMs
   *
   * @function updateAdomDeviceVdoms
   * @param {string} adom - the adom to update the device on (required)
   * @param {string} device - the device to update vdoms on (required)
   * @param {string} vdom - the specific vdom to update (option)
   * @param {array} vdomInfo - the vdom information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateAdomDeviceVdoms(adom, device, vdom, vdomInfo, callback) {
    const meth = 'adapter-updateAdomDeviceVdoms';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adom === undefined || adom === null || adom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (vdomInfo === undefined || vdomInfo === null || vdomInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['vdomInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'update',
      params: [
        {
          data: vdomInfo,
          url: `/dvmdb/adom/${adom}/device/${device}/vdom`
        }
      ]
    };
    if (vdom !== undefined && vdom !== null && vdom !== '') {
      body.params[0].url = `/dvmdb/adom/${adom}/device/${device}/vdom/${vdom}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to delete ADOM device VDOM
   *
   * @function deleteAdomDeviceVdom
   * @param {string} adom - the adom to delete the device on (required)
   * @param {string} device - the device to delete vdoms on (required)
   * @param {string} vdom - the specific vdom to set (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteAdomDeviceVdom(adom, device, vdom, callback) {
    const meth = 'adapter-deleteAdomDeviceVdom';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adom === undefined || adom === null || adom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (vdom === undefined || vdom === null || vdom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['vdom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'delete',
      params: [
        {
          url: `/dvmdb/adom/${adom}/device/${device}/vdom/${vdom}`
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to get Scripts
   *
   * @function getScripts
   * @param {string} script - the specific script to retrieve (option)
   * @param {string} expandMember - attributes of members to return (option)
   * @param {array} fields - limit the output by returning only the attributes specified in the string array (optional)
   * @param {array} filter - filter the result according to a set of criteria (optional)
   * @param {number} loadSub - sub objects to return (optional)
   * @param {string} option - set fetch option for the request (optional)
   * @param {array} range - limit the number of output (optional)
   * @param {array} sortings - sorting of the returned result (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getScripts(script, expandMember, fields, filter, loadSub, option, range, sortings, callback) {
    const meth = 'adapter-getScripts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU DEFINE VARIABLES */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: '/dvmdb/script'
        }
      ]
    };
    if (script !== undefined && script !== null && script !== '') {
      body.params[0].url = `/dvmdb/script/${script}`;
    }
    if (expandMember !== undefined && expandMember !== null && expandMember !== '') {
      body.params['expand member'] = expandMember;
    }
    if (fields !== undefined && fields !== null && fields !== '') {
      if (Array.isArray(fields)) {
        body.params[0].fields = fields;
      } else if (typeof fields === 'string') {
        body.params[0].fields = [fields];
      }
    }
    if (filter !== undefined && filter !== null && filter !== '') {
      if (Array.isArray(filter)) {
        body.params[0].filter = filter;
      } else if (typeof filter === 'string') {
        body.params[0].filter = [filter];
      }
    }
    if (loadSub !== undefined && loadSub !== null) {
      body.params[0].loadSub = loadSub;
    }
    if (option !== undefined && option !== null && option !== '') {
      body.params[0].option = option;
    }
    if (range !== undefined && range !== null && range !== '') {
      if (Array.isArray(range)) {
        body.params[0].range = range;
      } else if (typeof range === 'string') {
        body.params[0].range = [range];
      }
    }
    if (sortings !== undefined && sortings !== null && sortings !== '') {
      if (Array.isArray(sortings)) {
        body.params[0].sortings = sortings;
      } else if (typeof sortings === 'string') {
        body.params[0].sortings = [sortings];
      }
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create Scripts
   *
   * @function createScripts
   * @param {array} scriptInfo - the script information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createScripts(scriptInfo, callback) {
    const meth = 'adapter-createScripts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (scriptInfo === undefined || scriptInfo === null || scriptInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['scriptInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'add',
      params: [
        {
          data: scriptInfo,
          url: '/dvmdb/script'
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to set Scripts
   *
   * @function setScripts
   * @param {string} script - the specific script to set (option)
   * @param {array} scriptInfo - the script information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setScripts(script, scriptInfo, callback) {
    const meth = 'adapter-setScripts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (scriptInfo === undefined || scriptInfo === null || scriptInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['scriptInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'set',
      params: [
        {
          data: scriptInfo,
          url: '/dvmdb/script'
        }
      ]
    };
    if (script !== undefined && script !== null && script !== '') {
      body.params[0].url = `/dvmdb/script/${script}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to update Scripts
   *
   * @function updateScripts
   * @param {string} script - the specific script to update (optional)
   * @param {array} scriptInfo - the script information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateScripts(script, scriptInfo, callback) {
    const meth = 'adapter-updateScripts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (scriptInfo === undefined || scriptInfo === null || scriptInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['scriptInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'update',
      params: [
        {
          data: scriptInfo,
          url: '/dvmdb/script'
        }
      ]
    };
    if (script !== undefined && script !== null && script !== '') {
      body.params[0].url = `/dvmdb/script/${script}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to execute Scripts
   *
   * @function executeScripts
   * @param {array} scriptInfo - the script information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  executeScripts(scriptInfo, callback) {
    const meth = 'adapter-executeScripts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (scriptInfo === undefined || scriptInfo === null || scriptInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['scriptInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'exec',
      params: [
        {
          data: scriptInfo,
          url: '/dvmdb/script/execute'
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to delete Script
   *
   * @function deleteScript
   * @param {string} script - the specific script to delete (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteScript(script, callback) {
    const meth = 'adapter-deleteScript';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (script === undefined || script === null || script === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['script'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'delete',
      params: [
        {
          url: `/dvmdb/script/${script}`
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to get Global Scripts
   *
   * @function getGlobalScripts
   * @param {string} script - the specific script to retrieve (option)
   * @param {string} expandMember - attributes of members to return (option)
   * @param {array} fields - limit the output by returning only the attributes specified in the string array (optional)
   * @param {array} filter - filter the result according to a set of criteria (optional)
   * @param {number} loadSub - sub objects to return (optional)
   * @param {string} option - set fetch option for the request (optional)
   * @param {array} range - limit the number of output (optional)
   * @param {array} sortings - sorting of the returned result (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getGlobalScripts(script, expandMember, fields, filter, loadSub, option, range, sortings, callback) {
    const meth = 'adapter-getGlobalScripts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */

    /* HERE IS WHERE YOU DEFINE VARIABLES */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: '/dvmdb/global/script'
        }
      ]
    };
    if (script !== undefined && script !== null && script !== '') {
      body.params[0].url = `/dvmdb/global/script/${script}`;
    }
    if (expandMember !== undefined && expandMember !== null && expandMember !== '') {
      body.params['expand member'] = expandMember;
    }
    if (fields !== undefined && fields !== null && fields !== '') {
      if (Array.isArray(fields)) {
        body.params[0].fields = fields;
      } else if (typeof fields === 'string') {
        body.params[0].fields = [fields];
      }
    }
    if (filter !== undefined && filter !== null && filter !== '') {
      if (Array.isArray(filter)) {
        body.params[0].filter = filter;
      } else if (typeof filter === 'string') {
        body.params[0].filter = [filter];
      }
    }
    if (loadSub !== undefined && loadSub !== null) {
      body.params[0].loadSub = loadSub;
    }
    if (option !== undefined && option !== null && option !== '') {
      body.params[0].option = option;
    }
    if (range !== undefined && range !== null && range !== '') {
      if (Array.isArray(range)) {
        body.params[0].range = range;
      } else if (typeof range === 'string') {
        body.params[0].range = [range];
      }
    }
    if (sortings !== undefined && sortings !== null && sortings !== '') {
      if (Array.isArray(sortings)) {
        body.params[0].sortings = sortings;
      } else if (typeof sortings === 'string') {
        body.params[0].sortings = [sortings];
      }
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create Global Scripts
   *
   * @function createGlobalScripts
   * @param {array} scriptInfo - the script information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createGlobalScripts(scriptInfo, callback) {
    const meth = 'adapter-createGlobalScripts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (scriptInfo === undefined || scriptInfo === null || scriptInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['scriptInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'add',
      params: [
        {
          data: scriptInfo,
          url: '/dvmdb/global/script'
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to set Global Scripts
   *
   * @function setGlobalScripts
   * @param {string} script - the specific script to set (option)
   * @param {array} scriptInfo - the script information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setGlobalScripts(script, scriptInfo, callback) {
    const meth = 'adapter-setGlobalScripts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (scriptInfo === undefined || scriptInfo === null || scriptInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['scriptInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'set',
      params: [
        {
          data: scriptInfo,
          url: '/dvmdb/global/script'
        }
      ]
    };
    if (script !== undefined && script !== null && script !== '') {
      body.params[0].url = `/dvmdb/global/script/${script}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to update Global Scripts
   *
   * @function updateGlobalScripts
   * @param {string} script - the specific script to update (optional)
   * @param {array} scriptInfo - the script information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateGlobalScripts(script, scriptInfo, callback) {
    const meth = 'adapter-updateGlobalScripts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (scriptInfo === undefined || scriptInfo === null || scriptInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['scriptInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'update',
      params: [
        {
          data: scriptInfo,
          url: '/dvmdb/global/script'
        }
      ]
    };
    if (script !== undefined && script !== null && script !== '') {
      body.params[0].url = `/dvmdb/global/script/${script}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to execute Global Scripts
   *
   * @function executeGlobalScripts
   * @param {array} scriptInfo - the script information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  executeGlobalScripts(scriptInfo, callback) {
    const meth = 'adapter-executeGlobalScripts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (scriptInfo === undefined || scriptInfo === null || scriptInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['scriptInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'exec',
      params: [
        {
          data: scriptInfo,
          url: '/dvmdb/global/script/execute'
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to delete Global Script
   *
   * @function deleteGlobalScript
   * @param {string} script - the specific script to delete (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteGlobalScript(script, callback) {
    const meth = 'adapter-deleteGlobalScript';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (script === undefined || script === null || script === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['script'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'delete',
      params: [
        {
          url: `/dvmdb/global/script/${script}`
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to get ADOM Scripts
   *
   * @function getAdomScripts
   * @param {string} adom - the adom to get scripts from (required)
   * @param {string} script - the specific script to retrieve (option)
   * @param {string} expandMember - attributes of members to return (option)
   * @param {array} fields - limit the output by returning only the attributes specified in the string array (optional)
   * @param {array} filter - filter the result according to a set of criteria (optional)
   * @param {number} loadSub - sub objects to return (optional)
   * @param {string} option - set fetch option for the request (optional)
   * @param {array} range - limit the number of output (optional)
   * @param {array} sortings - sorting of the returned result (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getAdomScripts(adom, script, expandMember, fields, filter, loadSub, option, range, sortings, callback) {
    const meth = 'adapter-getAdomScripts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adom === undefined || adom === null || adom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU DEFINE VARIABLES */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: `/dvmdb/adom/${adom}/script`
        }
      ]
    };
    if (script !== undefined && script !== null && script !== '') {
      body.params[0].url = `/dvmdb/adom/${adom}/script/${script}`;
    }
    if (expandMember !== undefined && expandMember !== null && expandMember !== '') {
      body.params['expand member'] = expandMember;
    }
    if (fields !== undefined && fields !== null && fields !== '') {
      if (Array.isArray(fields)) {
        body.params[0].fields = fields;
      } else if (typeof fields === 'string') {
        body.params[0].fields = [fields];
      }
    }
    if (filter !== undefined && filter !== null && filter !== '') {
      if (Array.isArray(filter)) {
        body.params[0].filter = filter;
      } else if (typeof filter === 'string') {
        body.params[0].filter = [filter];
      }
    }
    if (loadSub !== undefined && loadSub !== null) {
      body.params[0].loadSub = loadSub;
    }
    if (option !== undefined && option !== null && option !== '') {
      body.params[0].option = option;
    }
    if (range !== undefined && range !== null && range !== '') {
      if (Array.isArray(range)) {
        body.params[0].range = range;
      } else if (typeof range === 'string') {
        body.params[0].range = [range];
      }
    }
    if (sortings !== undefined && sortings !== null && sortings !== '') {
      if (Array.isArray(sortings)) {
        body.params[0].sortings = sortings;
      } else if (typeof sortings === 'string') {
        body.params[0].sortings = [sortings];
      }
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create ADOM Scripts
   *
   * @function createAdomScripts
   * @param {string} adom - the adom to create scripts on (required)
   * @param {array} scriptInfo - the script information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createAdomScripts(adom, scriptInfo, callback) {
    const meth = 'adapter-createAdomScripts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adom === undefined || adom === null || adom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (scriptInfo === undefined || scriptInfo === null || scriptInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['scriptInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'add',
      params: [
        {
          data: scriptInfo,
          url: `/dvmdb/adom/${adom}/script`
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to set ADOM Scripts
   *
   * @function setAdomScripts
   * @param {string} adom - the adom to set scripts on (required)
   * @param {string} script - the specific script to set (option)
   * @param {array} scriptInfo - the script information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setAdomScripts(adom, script, scriptInfo, callback) {
    const meth = 'adapter-setAdomScripts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adom === undefined || adom === null || adom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (scriptInfo === undefined || scriptInfo === null || scriptInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['scriptInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'set',
      params: [
        {
          data: scriptInfo,
          url: `/dvmdb/adom/${adom}/script`
        }
      ]
    };
    if (script !== undefined && script !== null && script !== '') {
      body.params[0].url = `/dvmdb/adom/${adom}/script/${script}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to update ADOM Scripts
   *
   * @function updateAdomScripts
   * @param {string} adom - the adom to update scripts on (required)
   * @param {string} script - the specific script to update (optional)
   * @param {array} scriptInfo - the script information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateAdomScripts(adom, script, scriptInfo, callback) {
    const meth = 'adapter-updateAdomScripts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adom === undefined || adom === null || adom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (scriptInfo === undefined || scriptInfo === null || scriptInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['scriptInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'update',
      params: [
        {
          data: scriptInfo,
          url: `/dvmdb/adom/${adom}/script`
        }
      ]
    };
    if (script !== undefined && script !== null && script !== '') {
      body.params[0].url = `/dvmdb/adom/${adom}/script/${script}`;
    }

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to execute ADOM Scripts
   *
   * @function executeAdomScripts
   * @param {string} adom - the adom to execute scripts on (required)
   * @param {array} scriptInfo - the script information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  executeAdomScripts(adom, scriptInfo, callback) {
    const meth = 'adapter-executeAdomScripts';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adom === undefined || adom === null || adom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (scriptInfo === undefined || scriptInfo === null || scriptInfo === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['scriptInfo'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'exec',
      params: [
        {
          data: scriptInfo,
          url: `/dvmdb/adom/${adom}/script/execute`
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to delete ADOM Script
   *
   * @function deleteAdomScript
   * @param {string} adom - the adom to delete scripts from (required)
   * @param {string} script - the specific script to delete (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteAdomScript(adom, script, callback) {
    const meth = 'adapter-deleteAdomScript';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adom === undefined || adom === null || adom === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adom'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (script === undefined || script === null || script === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['script'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'delete',
      params: [
        {
          url: `/dvmdb/adom/${adom}/script/${script}`
        }
      ]
    };

    // make the call through the generic request
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create device settings
   *
   * @function createDeviceSettings
   * @param {object} settings - the settings for the device (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createDeviceSettings(settings, callback) {
    const meth = 'adapter-createDeviceSettings';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (settings === undefined || settings === null || settings === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['settings'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'exec',
      params: [
        {
          data: settings,
          url: '/securityconsole/install/device'
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create security settings
   *
   * @function createSecuritySettings
   * @param {object} settings - the settings for security (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createSecuritySettings(settings, callback) {
    const meth = 'adapter-createSecuritySettings';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (settings === undefined || settings === null || settings === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['settings'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'exec',
      params: [
        {
          data: settings,
          url: '/securityconsole/install/package'
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to monitor a task
   *
   * @function monitorTask
   * @param {string} taskid - the id of the task to monitor (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  monitorTask(taskid, callback) {
    const meth = 'adapter-monitorTask';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (taskid === undefined || taskid === null || taskid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['taskid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: `task/task/${taskid}`
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to execute device commands
   *
   * @function executeDeviceCommands
   * @param {string} device - the device to execute commands on (required)
   * @param {string} vdomid - the id of a specific vdom (optional) default global
   * @param {string} cli - the cli command to execute. Replace spaces with slashes (required)
   * @param {object} data - the data to send with the command (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  executeDeviceCommands(device, vdomid, cli, data, callback) {
    const meth = 'adapter-executeDeviceCommands';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (cli === undefined || cli === null || cli === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['cli'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'exec',
      params: [
        {
          url: `/pm/config/device/${device}/global/${cli}`
        }
      ]
    };
    if (vdomid !== undefined && vdomid !== null && vdomid !== '') {
      body.params[0].url = `/pm/config/device/${device}/vdom/${vdomid}/${cli}`;
    }
    if (data !== undefined && data !== null && data !== '') {
      body.params[0].data = data;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to get device interfaces
   *
   * @function getDeviceInterfaces
   * @param {string} device - the device to get interfaces on (required)
   * @param {string} vdomid - the id of a specific vdom (optional) default global
   * @param {string} iface - the id of a specific interface (optional)
   * @param {string} attr - the name of the attribute to retrieve its datasource (optional)
   * @param {array} fields - limit the output by returning only the attributes specified in the string array (optional)
   * @param {array} filter - filter the result according to a set of criteria (optional)
   * @param {array} option - set fetch option for the request (optional)
   * @param {array} range - limit the number of output (optional)
   * @param {array} sortings - sorting of the returned result (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getDeviceInterfaces(device, vdomid, iface, attr, fields, filter, option, range, sortings, callback) {
    const meth = 'adapter-getDeviceInterfaces';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: `/pm/config/device/${device}/global/system/interface`
        }
      ]
    };
    if (vdomid !== undefined && vdomid !== null && vdomid !== '') {
      body.params[0].url = `/pm/config/device/${device}/vdom/${vdomid}/system/interface`;
    }
    if (iface !== undefined && iface !== null && iface !== '') {
      body.params[0].url = `${body.params[0].url}/${iface}`;
    }
    if (attr !== undefined && attr !== null && attr !== '') {
      body.params[0].attr = attr;
    }
    if (fields !== undefined && fields !== null && fields !== '') {
      if (Array.isArray(fields)) {
        body.params[0].fields = fields;
      } else if (typeof fields === 'string') {
        body.params[0].fields = [fields];
      }
    }
    if (filter !== undefined && filter !== null && filter !== '') {
      if (Array.isArray(filter)) {
        body.params[0].filter = filter;
      } else if (typeof filter === 'string') {
        body.params[0].filter = [filter];
      }
    }
    if (option !== undefined && option !== null && option !== '') {
      if (Array.isArray(option)) {
        body.params[0].option = option;
      } else if (typeof option === 'string') {
        body.params[0].option = [option];
      }
    }
    if (range !== undefined && range !== null && range !== '') {
      if (Array.isArray(range)) {
        body.params[0].range = range;
      } else if (typeof range === 'string') {
        body.params[0].range = [range];
      }
    }
    if (sortings !== undefined && sortings !== null && sortings !== '') {
      if (Array.isArray(sortings)) {
        body.params[0].sortings = sortings;
      } else if (typeof sortings === 'string') {
        body.params[0].sortings = [sortings];
      }
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create a device interfaces
   *
   * @function createDeviceInterfaces
   * @param {string} device - the device to create interfaces on (required)
   * @param {string} vdomid - the id of a specific vdom (optional) default global
   * @param {object} devinterface - the device interface information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createDeviceInterfaces(device, vdomid, devinterface, callback) {
    const meth = 'adapter-createDeviceInterfaces';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (devinterface === undefined || devinterface === null || devinterface === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['devinterface'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'add',
      params: [
        {
          data: devinterface,
          url: `/pm/config/device/${device}/global/system/interface`
        }
      ]
    };
    if (vdomid !== undefined && vdomid !== null && vdomid !== '') {
      body.params[0].url = `/pm/config/device/${device}/vdom/${vdomid}/system/interface`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to clone a device interfaces
   *
   * @function cloneDeviceInterfaces
   * @param {string} device - the device to clone interfaces on (required)
   * @param {string} vdomid - the id of a specific vdom (optional) default global
   * @param {string} iface - the id of a specific interface (required)
   * @param {object} devinterface - the device interface information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cloneDeviceInterfaces(device, vdomid, iface, devinterface, callback) {
    const meth = 'adapter-cloneDeviceInterfaces';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (iface === undefined || iface === null || iface === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['iface'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (devinterface === undefined || devinterface === null || devinterface === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['devinterface'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'clone',
      params: [
        {
          data: devinterface,
          url: `/pm/config/device/${device}/global/system/interface/${iface}`
        }
      ]
    };
    if (vdomid !== undefined && vdomid !== null && vdomid !== '') {
      body.params[0].url = `/pm/config/device/${device}/vdom/${vdomid}/system/interface/${iface}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to update a device interfaces
   *
   * @function updateDeviceInterfaces
   * @param {string} device - the device to update interfaces on (required)
   * @param {string} vdomid - the id of a specific vdom (optional) default global
   * @param {string} iface - the id of a specific interface (optional)
   * @param {object} devinterface - the device interface information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateDeviceInterfaces(device, vdomid, iface, devinterface, callback) {
    const meth = 'adapter-updateDeviceInterfaces';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (devinterface === undefined || devinterface === null || devinterface === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['devinterface'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'update',
      params: [
        {
          data: devinterface,
          url: `/pm/config/device/${device}/global/system/interface`
        }
      ]
    };
    if (vdomid !== undefined && vdomid !== null && vdomid !== '') {
      body.params[0].url = `/pm/config/device/${device}/vdom/${vdomid}/system/interface`;
    }
    if (iface !== undefined && iface !== null && iface !== '') {
      body.params[0].url = `${body.params[0].url}/${iface}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to set a device interfaces
   *
   * @function setDeviceInterfaces
   * @param {string} device - the device to set interface on (required)
   * @param {string} vdomid - the id of a specific vdom (optional) default global
   * @param {string} iface - the id of a specific interface (optional)
   * @param {object} devinterface - the device interface information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setDeviceInterfaces(device, vdomid, iface, devinterface, callback) {
    const meth = 'adapter-setDeviceInterfaces';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (devinterface === undefined || devinterface === null || devinterface === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['devinterface'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'set',
      params: [
        {
          data: devinterface,
          url: `/pm/config/device/${device}/global/system/interface`
        }
      ]
    };
    if (vdomid !== undefined && vdomid !== null && vdomid !== '') {
      body.params[0].url = `/pm/config/device/${device}/vdom/${vdomid}/system/interface`;
    }
    if (iface !== undefined && iface !== null && iface !== '') {
      body.params[0].url = `${body.params[0].url}/${iface}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to delete a device interfaces
   *
   * @function deleteDeviceInterfaces
   * @param {string} device - the device to delete interface on (required)
   * @param {string} vdomid - the id of a specific vdom (optional) default global
   * @param {string} iface - the id of a specific interface (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteDeviceInterfaces(device, vdomid, iface, callback) {
    const meth = 'adapter-deleteDeviceInterfaces';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (iface === undefined || iface === null || iface === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['iface'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'delete',
      params: [
        {
          url: `/pm/config/device/${device}/global/system/interface/${iface}`
        }
      ]
    };
    if (vdomid !== undefined && vdomid !== null && vdomid !== '') {
      body.params[0].url = `/pm/config/device/${device}/vdom/${vdomid}/system/interface/${iface}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to get device static routes
   *
   * @function getDeviceStaticRoutes
   * @param {string} device - the device to get static routes on (required)
   * @param {string} vdomid - the id of a specific vdom (optional) default global
   * @param {string} route - the id of a specific route (optional)
   * @param {string} attr - the name of the attribute to retrieve its datasource (optional)
   * @param {array} fields - limit the output by returning only the attributes specified in the string array (optional)
   * @param {array} filter - filter the result according to a set of criteria (optional)
   * @param {array} option - set fetch option for the request (optional)
   * @param {array} range - set fetch option for the request (optional)
   * @param {array} sortings - set fetch option for the request (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getDeviceStaticRoutes(device, vdomid, route, attr, fields, filter, option, range, sortings, callback) {
    const meth = 'adapter-getDeviceStaticRoutes';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: `/pm/config/device/${device}/global/router/static`
        }
      ]
    };
    if (vdomid !== undefined && vdomid !== null && vdomid !== '') {
      body.params[0].url = `/pm/config/device/${device}/vdom/${vdomid}/router/static`;
    }
    if (route !== undefined && route !== null && route !== '') {
      body.params[0].url = `${body.params[0].url}/${route}`;
    }
    if (attr !== undefined && attr !== null && attr !== '') {
      body.params[0].attr = attr;
    }
    if (fields !== undefined && fields !== null && fields !== '') {
      if (Array.isArray(fields)) {
        body.params[0].fields = fields;
      } else if (typeof fields === 'string') {
        body.params[0].fields = [fields];
      }
    }
    if (filter !== undefined && filter !== null && filter !== '') {
      if (Array.isArray(filter)) {
        body.params[0].filter = filter;
      } else if (typeof filter === 'string') {
        body.params[0].filter = [filter];
      }
    }
    if (option !== undefined && option !== null && option !== '') {
      if (Array.isArray(option)) {
        body.params[0].option = option;
      } else if (typeof option === 'string') {
        body.params[0].option = [option];
      }
    }
    if (range !== undefined && range !== null && range !== '') {
      if (Array.isArray(range)) {
        body.params[0].range = range;
      } else if (typeof range === 'string') {
        body.params[0].range = [range];
      }
    }
    if (sortings !== undefined && sortings !== null && sortings !== '') {
      if (Array.isArray(sortings)) {
        body.params[0].sortings = sortings;
      } else if (typeof sortings === 'string') {
        body.params[0].sortings = [sortings];
      }
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create a device static routes
   *
   * @function createDeviceStaticRoutes
   * @param {string} device - the device to create static routes on (required)
   * @param {string} vdomid - the id of a specific vdom (optional) default global
   * @param {object} devroute - the device route information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createDeviceStaticRoutes(device, vdomid, devroute, callback) {
    const meth = 'adapter-createDeviceStaticRoutes';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (devroute === undefined || devroute === null || devroute === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['devroute'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'add',
      params: [
        {
          data: devroute,
          url: `/pm/config/device/${device}/global/router/static`
        }
      ]
    };
    if (vdomid !== undefined && vdomid !== null && vdomid !== '') {
      body.params[0].url = `/pm/config/device/${device}/vdom/${vdomid}/router/static`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to clone a statuc routes
   *
   * @function cloneDeviceStaticRoutes
   * @param {string} device - the device to clone static routes on (required)
   * @param {string} vdomid - the id of a specific vdom (optional) default global
   * @param {string} route - the id of a specific route (required)
   * @param {object} devroute - the device route information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cloneDeviceStaticRoutes(device, vdomid, route, devroute, callback) {
    const meth = 'adapter-cloneDeviceStaticRoutes';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (route === undefined || route === null || route === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['route'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (devroute === undefined || devroute === null || devroute === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['devroute'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'clone',
      params: [
        {
          data: devroute,
          url: `/pm/config/device/${device}/global/router/static/${route}`
        }
      ]
    };
    if (vdomid !== undefined && vdomid !== null && vdomid !== '') {
      body.params[0].url = `/pm/config/device/${device}/vdom/${vdomid}/router/static/${route}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to update a static routes
   *
   * @function updateDeviceStaticRoutes
   * @param {string} device - the device to update static routes on (required)
   * @param {string} vdomid - the id of a specific vdom (optional) default global
   * @param {string} route - the id of a specific route (optional)
   * @param {object} devroute - the device route information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateDeviceStaticRoutes(device, vdomid, route, devroute, callback) {
    const meth = 'adapter-updateDeviceStaticRoutes';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (devroute === undefined || devroute === null || devroute === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['devroute'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'update',
      params: [
        {
          data: devroute,
          url: `/pm/config/device/${device}/global/router/static`
        }
      ]
    };
    if (vdomid !== undefined && vdomid !== null && vdomid !== '') {
      body.params[0].url = `/pm/config/device/${device}/vdom/${vdomid}/router/static`;
    }
    if (route !== undefined && route !== null && route !== '') {
      body.params[0].url = `${body.params[0].url}/${route}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to set a device static routes
   *
   * @function setDeviceStaticRoutes
   * @param {string} device - the device to set static routes on (required)
   * @param {string} vdomid - the id of a specific vdom (optional) default global
   * @param {string} route - the id of a specific route (optional)
   * @param {object} devroute - the device route information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setDeviceStaticRoutes(device, vdomid, route, devroute, callback) {
    const meth = 'adapter-setDeviceStaticRoutes';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (devroute === undefined || devroute === null || devroute === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['devroute'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'set',
      params: [
        {
          data: devroute,
          url: `/pm/config/device/${device}/global/router/static`
        }
      ]
    };
    if (vdomid !== undefined && vdomid !== null && vdomid !== '') {
      body.params[0].url = `/pm/config/device/${device}/vdom/${vdomid}/router/static`;
    }
    if (route !== undefined && route !== null && route !== '') {
      body.params[0].url = `${body.params[0].url}/${route}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to delete a static routes
   *
   * @function deleteStaticRoutes
   * @param {string} device - the device to delete static routes on (required)
   * @param {string} vdomid - the id of a specific vdom (optional) default global
   * @param {string} route - the id of a specific route (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteStaticRoutes(device, vdomid, route, callback) {
    const meth = 'adapter-deleteStaticRoutes';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (device === undefined || device === null || device === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['device'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (route === undefined || route === null || route === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['route'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'delete',
      params: [
        {
          url: `/pm/config/device/${device}/global/router/static/${route}`
        }
      ]
    };
    if (vdomid !== undefined && vdomid !== null && vdomid !== '') {
      body.params[0].url = `/pm/config/device/${device}/vdom/${vdomid}/router/static/${route}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to get firewall addresses
   *
   * @function getFirewallAddresses
   * @param {string} adomid - id of a specific adom (optional) default global
   * @param {string} address - id of a specific adom (optional) default all
   * @param {string} attr - the name of the attribute to retrieve its datasource (optional)
   * @param {array} fields - limit the output by returning only the attributes specified in the string array (optional)
   * @param {array} filter - filter the result according to a set of criteria (optional)
   * @param {array} option - set fetch option for the request (optional)
   * @param {array} range - set fetch option for the request (optional)
   * @param {array} sortings - set fetch option for the request (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getFirewallAddresses(adomid, address, attr, fields, filter, option, range, sortings, callback) {
    const meth = 'adapter-getFirewallAddresses';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: '/pm/config/global/obj/firewall/address'
        }
      ]
    };
    if (adomid !== undefined && adomid !== null && adomid !== '') {
      body.params[0].url = `/pm/config/adom/${adomid}/obj/firewall/address`;
    }
    if (address !== undefined && address !== null && address !== '') {
      body.params[0].url = `${body.params[0].url}/${address}`;
    }
    if (attr !== undefined && attr !== null && attr !== '') {
      body.params[0].attr = attr;
    }
    if (fields !== undefined && fields !== null && fields !== '') {
      if (Array.isArray(fields)) {
        body.params[0].fields = fields;
      } else if (typeof fields === 'string') {
        body.params[0].fields = [fields];
      }
    }
    if (filter !== undefined && filter !== null && filter !== '') {
      if (Array.isArray(filter)) {
        body.params[0].filter = filter;
      } else if (typeof filter === 'string') {
        body.params[0].filter = [filter];
      }
    }
    if (option !== undefined && option !== null && option !== '') {
      if (Array.isArray(option)) {
        body.params[0].option = option;
      } else if (typeof option === 'string') {
        body.params[0].option = [option];
      }
    }
    if (range !== undefined && range !== null && range !== '') {
      if (Array.isArray(range)) {
        body.params[0].range = range;
      } else if (typeof range === 'string') {
        body.params[0].range = [range];
      }
    }
    if (sortings !== undefined && sortings !== null && sortings !== '') {
      if (Array.isArray(sortings)) {
        body.params[0].sortings = sortings;
      } else if (typeof sortings === 'string') {
        body.params[0].sortings = [sortings];
      }
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create a firewall addresses
   *
   * @function createFirewallAddresses
   * @param {string} adomid - id of a specific adom to add the firewall address (optional) default global
   * @param {object} fwaddress - the firewall address information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createFirewallAddresses(adomid, fwaddress, callback) {
    const meth = 'adapter-createFirewallAddresses';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (fwaddress === undefined || fwaddress === null || fwaddress === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['fwaddress'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'add',
      params: [
        {
          data: fwaddress,
          url: '/pm/config/global/obj/firewall/address'
        }
      ]
    };
    if (adomid !== undefined && adomid !== null && adomid !== '') {
      body.params[0].url = `/pm/config/adom/${adomid}/obj/firewall/address`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to clone a firewall addresses
   *
   * @function cloneFirewallAddresses
   * @param {string} adomid - id of a specific adom to clone the firewall address (optional) default global
   * @param {string} address - id of a specific firewall address to clone (required)
   * @param {object} fwaddress - the firewall address information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cloneFirewallAddresses(adomid, address, fwaddress, callback) {
    const meth = 'adapter-cloneFirewallAddresses';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (address === undefined || address === null || address === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['address'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (fwaddress === undefined || fwaddress === null || fwaddress === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['fwaddress'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'clone',
      params: [
        {
          data: fwaddress,
          url: `/pm/config/global/obj/firewall/address/${address}`
        }
      ]
    };
    if (adomid !== undefined && adomid !== null && adomid !== '') {
      body.params[0].url = `/pm/config/adom/${adomid}/obj/firewall/address/${address}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to update a firewall addresses
   *
   * @function updateFirewallAddresses
   * @param {string} adomid - id of a specific adom to update the firewall address (optional) default global
   * @param {string} address - id of a specific firewall address to update (optional) if not provide in fwaddress
   * @param {object} fwaddress - the firewall address information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateFirewallAddresses(adomid, address, fwaddress, callback) {
    const meth = 'adapter-updateFirewallAddresses';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (fwaddress === undefined || fwaddress === null || fwaddress === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['fwaddress'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'update',
      params: [
        {
          data: fwaddress,
          url: '/pm/config/global/obj/firewall/address'
        }
      ]
    };
    if (adomid !== undefined && adomid !== null && adomid !== '') {
      body.params[0].url = `/pm/config/adom/${adomid}/obj/firewall/address`;
    }
    if (address !== undefined && address !== null && address !== '') {
      body.params[0].url = `${body.params[0].url}/${address}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to set a firewall addresses
   *
   * @function setFirewallAddresses
   * @param {string} adomid - id of a specific adom to set the firewall address (optional) default global
   * @param {string} address - id of a specific firewall address to set (optional) if not provide in fwaddress
   * @param {object} fwaddress - the firewall address information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setFirewallAddresses(adomid, address, fwaddress, callback) {
    const meth = 'adapter-setFirewallAddresses';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (fwaddress === undefined || fwaddress === null || fwaddress === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['fwaddress'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'set',
      params: [
        {
          data: fwaddress,
          url: '/pm/config/global/obj/firewall/address'
        }
      ]
    };
    if (adomid !== undefined && adomid !== null && adomid !== '') {
      body.params[0].url = `/pm/config/adom/${adomid}/obj/firewall/address`;
    }
    if (address !== undefined && address !== null && address !== '') {
      body.params[0].url = `${body.params[0].url}/${address}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to delete a firewall addresses
   *
   * @function deleteFirewallAddresses
   * @param {string} adomid - id of a specific adom to delete the firewall address (optional) default global
   * @param {string} address - id of a specific firewall address to delete (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteFirewallAddresses(adomid, address, callback) {
    const meth = 'adapter-deleteFirewallAddresses';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (address === undefined || address === null || address === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['address'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'delete',
      params: [
        {
          url: `/pm/config/global/obj/firewall/address/${address}`
        }
      ]
    };
    if (adomid !== undefined && adomid !== null && adomid !== '') {
      body.params[0].url = `/pm/config/adom/${adomid}/obj/firewall/address/${address}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to get firewall policy package
   *
   * @function getFirewallPolicyPackage
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} packid - id of a specific pacakge (required)
   * @param {string} policy - id of a specific policy (optional) default all
   * @param {string} attr - the name of the attribute to retrieve its datasource (optional)
   * @param {array} fields - limit the output by returning only the attributes specified in the string array (optional)
   * @param {array} filter - filter the result according to a set of criteria (optional)
   * @param {array} option - set fetch option for the request (optional)
   * @param {array} range - set fetch option for the request (optional)
   * @param {array} sortings - set fetch option for the request (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getFirewallPolicyPackage(adomid, packid, policy, attr, fields, filter, option, range, sortings, callback) {
    const meth = 'adapter-getFirewallPolicyPackage';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (packid === undefined || packid === null || packid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['packid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* set up the specific call */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: `/pm/config/adom/${adomid}/pkg/${packid}/firewall/policy`
        }
      ]
    };
    if (policy !== undefined && policy !== null && policy !== '') {
      body.params[0].url = `${body.params[0].url}/${policy}`;
    }
    if (attr !== undefined && attr !== null && attr !== '') {
      body.params[0].attr = attr;
    }
    if (fields !== undefined && fields !== null && fields !== '') {
      if (Array.isArray(fields)) {
        body.params[0].fields = fields;
      } else if (typeof fields === 'string') {
        body.params[0].fields = [fields];
      }
    }
    if (filter !== undefined && filter !== null && filter !== '') {
      if (Array.isArray(filter)) {
        body.params[0].filter = filter;
      } else if (typeof filter === 'string') {
        body.params[0].filter = [filter];
      }
    }
    if (option !== undefined && option !== null && option !== '') {
      if (Array.isArray(option)) {
        body.params[0].option = option;
      } else if (typeof option === 'string') {
        body.params[0].option = [option];
      }
    }
    if (range !== undefined && range !== null && range !== '') {
      if (Array.isArray(range)) {
        body.params[0].range = range;
      } else if (typeof range === 'string') {
        body.params[0].range = [range];
      }
    }
    if (sortings !== undefined && sortings !== null && sortings !== '') {
      if (Array.isArray(sortings)) {
        body.params[0].sortings = sortings;
      } else if (typeof sortings === 'string') {
        body.params[0].sortings = [sortings];
      }
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create a firewall policy package
   *
   * @function createFirewallPolicyPackage
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} packid - id of a specific pacakge (required)
   * @param {object} fwpolicy - the firewall policy information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createFirewallPolicyPackage(adomid, packid, fwpolicy, callback) {
    const meth = 'adapter-createFirewallPolicyPackage';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (packid === undefined || packid === null || packid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['packid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (fwpolicy === undefined || fwpolicy === null || fwpolicy === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['fwpolicy'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'add',
      params: [
        {
          data: fwpolicy,
          url: `/pm/config/adom/${adomid}/pkg/${packid}/firewall/policy`
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to clone a firewall policy pacakage
   *
   * @function cloneFirewallPolicyPackage
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} packid - id of a specific pacakge (required)
   * @param {string} policy - id of a specific policy (required)
   * @param {object} fwpolicy - the firewall policy information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cloneFirewallPolicyPackage(adomid, packid, policy, fwpolicy, callback) {
    const meth = 'adapter-cloneFirewallPolicyPackage';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (packid === undefined || packid === null || packid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['packid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (policy === undefined || policy === null || policy === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['policy'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (fwpolicy === undefined || fwpolicy === null || fwpolicy === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['fwpolicy'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'clone',
      params: [
        {
          data: fwpolicy,
          url: `/pm/config/adom/${adomid}/pkg/${packid}/firewall/policy/${policy}`
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to update a firewall policy package
   *
   * @function updateFirewallPolicyPackage
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} packid - id of a specific pacakge (required)
   * @param {string} policy - id of a specific policy (optional)
   * @param {object} fwpolicy - the firewall policy information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateFirewallPolicyPackage(adomid, packid, policy, fwpolicy, callback) {
    const meth = 'adapter-updateFirewallPolicyPackage';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (packid === undefined || packid === null || packid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['fwaddress'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (fwpolicy === undefined || fwpolicy === null || fwpolicy === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['fwpolicy'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'update',
      params: [
        {
          data: fwpolicy,
          url: `/pm/config/adom/${adomid}/pkg/${packid}/firewall/policy`
        }
      ]
    };
    if (policy !== undefined && policy !== null && policy !== '') {
      body.params[0].url = `${body.params[0].url}/${policy}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to set a firewall policy packages
   *
   * @function setFirewallPolicyPackage
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} packid - id of a specific pacakge (required)
   * @param {string} policy - id of a specific policy (optional)
   * @param {object} fwpolicy - the firewall policy information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setFirewallPolicyPackage(adomid, packid, policy, fwpolicy, callback) {
    const meth = 'adapter-setFirewallPolicyPackage';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (packid === undefined || packid === null || packid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['fwaddress'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (fwpolicy === undefined || fwpolicy === null || fwpolicy === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['fwpolicy'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'set',
      params: [
        {
          data: fwpolicy,
          url: `/pm/config/adom/${adomid}/pkg/${packid}/firewall/policy`
        }
      ]
    };
    if (policy !== undefined && policy !== null && policy !== '') {
      body.params[0].url = `${body.params[0].url}/${policy}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to move a firewall policy packages
   *
   * @function moveFirewallPolicyPackage
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} packid - id of a specific pacakge (required)
   * @param {string} policy - id of a specific policy (required)
   * @param {string} target - key to the target entry (required)
   * @param {array} option - set option for the request (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  moveFirewallPolicyPackage(adomid, packid, policy, target, option, callback) {
    const meth = 'adapter-moveFirewallPolicyPackage';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (packid === undefined || packid === null || packid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['fwaddress'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (policy === undefined || policy === null || policy === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['policy'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (target === undefined || target === null || target === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['target'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'move',
      params: [
        {
          target,
          url: `/pm/config/adom/${adomid}/pkg/${packid}/firewall/policy/${policy}`
        }
      ]
    };
    if (option !== undefined && option !== null && option !== '') {
      if (Array.isArray(option)) {
        body.params[0].option = option;
      } else if (typeof option === 'string') {
        body.params[0].option = [option];
      }
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to delete a firewall policy package
   *
   * @function deleteFirewallPolicyPacakge
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} packid - id of a specific pacakge (required)
   * @param {string} policy - id of a specific policy (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteFirewallPolicyPacakge(adomid, packid, policy, callback) {
    const meth = 'adapter-deleteFirewallPolicyPacakge';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (packid === undefined || packid === null || packid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['fwaddress'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (policy === undefined || policy === null || policy === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['policy'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'delete',
      params: [
        {
          url: `/pm/config/adom/${adomid}/pkg/${packid}/firewall/policy/${policy}`
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to get sdwan internet connection
   *
   * @function getSDWANInternetConnection
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {string} attr - the name of the attribute to retrieve its datasource (optional)
   * @param {array} fields - limit the output by returning only the attributes specified in the string array (optional)
   * @param {array} filter - filter the result according to a set of criteria (optional)
   * @param {array} option - set fetch option for the request (optional)
   * @param {array} range - set fetch option for the request (optional)
   * @param {array} sortings - set fetch option for the request (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getSDWANInternetConnection(adomid, wanprof, attr, fields, filter, option, range, sortings, callback) {
    const meth = 'adapter-getSDWANInternetConnection';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* set up the specific call */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan`
        }
      ]
    };
    if (attr !== undefined && attr !== null && attr !== '') {
      body.params[0].attr = attr;
    }
    if (fields !== undefined && fields !== null && fields !== '') {
      if (Array.isArray(fields)) {
        body.params[0].fields = fields;
      } else if (typeof fields === 'string') {
        body.params[0].fields = [fields];
      }
    }
    if (filter !== undefined && filter !== null && filter !== '') {
      if (Array.isArray(filter)) {
        body.params[0].filter = filter;
      } else if (typeof filter === 'string') {
        body.params[0].filter = [filter];
      }
    }
    if (option !== undefined && option !== null && option !== '') {
      if (Array.isArray(option)) {
        body.params[0].option = option;
      } else if (typeof option === 'string') {
        body.params[0].option = [option];
      }
    }
    if (range !== undefined && range !== null && range !== '') {
      if (Array.isArray(range)) {
        body.params[0].range = range;
      } else if (typeof range === 'string') {
        body.params[0].range = [range];
      }
    }
    if (sortings !== undefined && sortings !== null && sortings !== '') {
      if (Array.isArray(sortings)) {
        body.params[0].sortings = sortings;
      } else if (typeof sortings === 'string') {
        body.params[0].sortings = [sortings];
      }
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to set a sdwan internet connection
   *
   * @function setSDWANInternetConnection
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {object} intconn - the internet connection information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setSDWANInternetConnection(adomid, wanprof, intconn, callback) {
    const meth = 'adapter-setSDWANInternetConnection';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (intconn === undefined || intconn === null || intconn === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['intconn'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'set',
      params: [
        {
          data: intconn,
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan`
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to update a sdwan internet connection
   *
   * @function updateSDWANInternetConnection
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {object} intconn - the internet connection information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateSDWANInternetConnection(adomid, wanprof, intconn, callback) {
    const meth = 'adapter-updateSDWANInternetConnection';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (intconn === undefined || intconn === null || intconn === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['intconn'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'update',
      params: [
        {
          data: intconn,
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan`
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to get sdwan neighbor
   *
   * @function getSDWANNeighbor
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {string} neighbor - id of a specific neighbor (optional)
   * @param {string} attr - the name of the attribute to retrieve its datasource (optional)
   * @param {array} fields - limit the output by returning only the attributes specified in the string array (optional)
   * @param {array} filter - filter the result according to a set of criteria (optional)
   * @param {array} option - set fetch option for the request (optional)
   * @param {array} range - set fetch option for the request (optional)
   * @param {array} sortings - set fetch option for the request (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getSDWANNeighbor(adomid, wanprof, neighbor, attr, fields, filter, option, range, sortings, callback) {
    const meth = 'adapter-getSDWANNeighbor';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* set up the specific call */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan/neighbor`
        }
      ]
    };
    if (neighbor !== undefined && neighbor !== null && neighbor !== '') {
      body.params[0].url = `${body.params[0].url}/${neighbor}`;
    }
    if (attr !== undefined && attr !== null && attr !== '') {
      body.params[0].attr = attr;
    }
    if (fields !== undefined && fields !== null && fields !== '') {
      if (Array.isArray(fields)) {
        body.params[0].fields = fields;
      } else if (typeof fields === 'string') {
        body.params[0].fields = [fields];
      }
    }
    if (filter !== undefined && filter !== null && filter !== '') {
      if (Array.isArray(filter)) {
        body.params[0].filter = filter;
      } else if (typeof filter === 'string') {
        body.params[0].filter = [filter];
      }
    }
    if (option !== undefined && option !== null && option !== '') {
      if (Array.isArray(option)) {
        body.params[0].option = option;
      } else if (typeof option === 'string') {
        body.params[0].option = [option];
      }
    }
    if (range !== undefined && range !== null && range !== '') {
      if (Array.isArray(range)) {
        body.params[0].range = range;
      } else if (typeof range === 'string') {
        body.params[0].range = [range];
      }
    }
    if (sortings !== undefined && sortings !== null && sortings !== '') {
      if (Array.isArray(sortings)) {
        body.params[0].sortings = sortings;
      } else if (typeof sortings === 'string') {
        body.params[0].sortings = [sortings];
      }
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create a sdwan neighbor
   *
   * @function createSDWANNeighbor
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {object} sdwanneighbor - the sdwan neighbor information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createSDWANNeighbor(adomid, wanprof, sdwanneighbor, callback) {
    const meth = 'adapter-createSDWANNeighbor';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (sdwanneighbor === undefined || sdwanneighbor === null || sdwanneighbor === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['sdwanneighbor'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'add',
      params: [
        {
          data: sdwanneighbor,
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan/neighbor`
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to clone a sdwan neighbor
   *
   * @function cloneSDWANNeighbor
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {string} neighbor - id of a specific neighbor (required)
   * @param {object} sdwanneighbor - the sdwan neighbor information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cloneSDWANNeighbor(adomid, wanprof, neighbor, sdwanneighbor, callback) {
    const meth = 'adapter-cloneSDWANNeighbor';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (neighbor === undefined || neighbor === null || neighbor === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['neighbor'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (sdwanneighbor === undefined || sdwanneighbor === null || sdwanneighbor === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['sdwanneighbor'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'clone',
      params: [
        {
          data: sdwanneighbor,
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan/neighbor/${neighbor}`
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to update a sdwan neighbor
   *
   * @function updateSDWANNeighbor
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {string} neighbor - id of a specific neighbor (optional)
   * @param {object} sdwanneighbor - the sdwan neighbor information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateSDWANNeighbor(adomid, wanprof, neighbor, sdwanneighbor, callback) {
    const meth = 'adapter-updateSDWANNeighbor';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (sdwanneighbor === undefined || sdwanneighbor === null || sdwanneighbor === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['sdwanneighbor'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'update',
      params: [
        {
          data: sdwanneighbor,
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan/neighbor`
        }
      ]
    };
    if (neighbor !== undefined && neighbor !== null && neighbor !== '') {
      body.params[0].url = `${body.params[0].url}/${neighbor}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to set a sdwan neighbor
   *
   * @function setSDWANNeighbor
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {string} neighbor - id of a specific neighbor (optionaL)
   * @param {object} sdwanneighbor - the sdwan neighbor information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setSDWANNeighbor(adomid, wanprof, neighbor, sdwanneighbor, callback) {
    const meth = 'adapter-setSDWANNeighbor';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (sdwanneighbor === undefined || sdwanneighbor === null || sdwanneighbor === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['sdwanneighbor'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'set',
      params: [
        {
          data: sdwanneighbor,
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan/neighbor`
        }
      ]
    };
    if (neighbor !== undefined && neighbor !== null && neighbor !== '') {
      body.params[0].url = `${body.params[0].url}/${neighbor}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to delete a sdwan neighbor
   *
   * @function deleteSDWANNeighbor
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {string} neighbor - id of a specific neighbor (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteSDWANNeighbor(adomid, wanprof, neighbor, callback) {
    const meth = 'adapter-deleteSDWANNeighbor';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (neighbor === undefined || neighbor === null || neighbor === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['neighbor'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'delete',
      params: [
        {
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan/neighbor/${neighbor}`
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to get sdwan service
   *
   * @function getSDWANService
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {string} service - id of a specific service (optional)
   * @param {string} attr - the name of the attribute to retrieve its datasource (optional)
   * @param {array} fields - limit the output by returning only the attributes specified in the string array (optional)
   * @param {array} filter - filter the result according to a set of criteria (optional)
   * @param {array} option - set fetch option for the request (optional)
   * @param {array} range - set fetch option for the request (optional)
   * @param {array} sortings - set fetch option for the request (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  getSDWANService(adomid, wanprof, service, attr, fields, filter, option, range, sortings, callback) {
    const meth = 'adapter-getSDWANService';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* set up the specific call */
    const body = {
      id: 1,
      method: 'get',
      params: [
        {
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan/service`
        }
      ]
    };
    if (service !== undefined && service !== null && service !== '') {
      body.params[0].url = `${body.params[0].url}/${service}`;
    }
    if (attr !== undefined && attr !== null && attr !== '') {
      body.params[0].attr = attr;
    }
    if (fields !== undefined && fields !== null && fields !== '') {
      if (Array.isArray(fields)) {
        body.params[0].fields = fields;
      } else if (typeof fields === 'string') {
        body.params[0].fields = [fields];
      }
    }
    if (filter !== undefined && filter !== null && filter !== '') {
      if (Array.isArray(filter)) {
        body.params[0].filter = filter;
      } else if (typeof filter === 'string') {
        body.params[0].filter = [filter];
      }
    }
    if (option !== undefined && option !== null && option !== '') {
      if (Array.isArray(option)) {
        body.params[0].option = option;
      } else if (typeof option === 'string') {
        body.params[0].option = [option];
      }
    }
    if (range !== undefined && range !== null && range !== '') {
      if (Array.isArray(range)) {
        body.params[0].range = range;
      } else if (typeof range === 'string') {
        body.params[0].range = [range];
      }
    }
    if (sortings !== undefined && sortings !== null && sortings !== '') {
      if (Array.isArray(sortings)) {
        body.params[0].sortings = sortings;
      } else if (typeof sortings === 'string') {
        body.params[0].sortings = [sortings];
      }
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create a sdwan service
   *
   * @function createSDWANService
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {object} sdwanservice - the sdwan service information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createSDWANService(adomid, wanprof, sdwanservice, callback) {
    const meth = 'adapter-createSDWANService';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (sdwanservice === undefined || sdwanservice === null || sdwanservice === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['sdwanservice'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'add',
      params: [
        {
          data: sdwanservice,
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan/service`
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to clone a sdwan service
   *
   * @function cloneSDWANService
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {string} service - id of a specific service (required)
   * @param {object} sdwanservice - the sdwan service information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cloneSDWANService(adomid, wanprof, service, sdwanservice, callback) {
    const meth = 'adapter-cloneSDWANService';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (service === undefined || service === null || service === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['service'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (sdwanservice === undefined || sdwanservice === null || sdwanservice === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['sdwanservice'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'clone',
      params: [
        {
          data: sdwanservice,
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan/service/${service}`
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to update a sdwan service
   *
   * @function updateSDWANService
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {string} service - id of a specific service (optional)
   * @param {object} sdwanservice - the sdwan service information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  updateSDWANService(adomid, wanprof, service, sdwanservice, callback) {
    const meth = 'adapter-updateSDWANService';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (sdwanservice === undefined || sdwanservice === null || sdwanservice === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['sdwanservice'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'update',
      params: [
        {
          data: sdwanservice,
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan/service`
        }
      ]
    };
    if (service !== undefined && service !== null && service !== '') {
      body.params[0].url = `${body.params[0].url}/${service}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to set a sdwan service
   *
   * @function setSDWANService
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {string} service - id of a specific service (optionaL)
   * @param {object} sdwanservice - the sdwan service information (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  setSDWANService(adomid, wanprof, service, sdwanservice, callback) {
    const meth = 'adapter-setSDWANService';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (sdwanservice === undefined || sdwanservice === null || sdwanservice === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['sdwanservice'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'set',
      params: [
        {
          data: sdwanservice,
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan/service`
        }
      ]
    };
    if (service !== undefined && service !== null && service !== '') {
      body.params[0].url = `${body.params[0].url}/${service}`;
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to move a sdwan service
   *
   * @function moveSDWANService
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {string} service - id of a specific service (required)
   * @param {string} target - key to the target entry (required)
   * @param {array} option - set option for the request (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  moveSDWANService(adomid, wanprof, service, target, option, callback) {
    const meth = 'adapter-moveSDWANService';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (service === undefined || service === null || service === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['service'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (target === undefined || target === null || target === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['target'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'move',
      params: [
        {
          target,
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan/service/${service}`
        }
      ]
    };
    if (option !== undefined && option !== null && option !== '') {
      if (Array.isArray(option)) {
        body.params[0].option = option;
      } else if (typeof option === 'string') {
        body.params[0].option = [option];
      }
    }
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to delete a sdwan service
   *
   * @function deleteSDWANService
   * @param {string} adomid - id of a specific adom (required)
   * @param {string} wanprof - id of a specific wanprof (required)
   * @param {string} service - id of a specific service (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteSDWANService(adomid, wanprof, service, callback) {
    const meth = 'adapter-deleteSDWANService';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (wanprof === undefined || wanprof === null || wanprof === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['wanprof'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (service === undefined || service === null || service === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['service'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'delete',
      params: [
        {
          url: `/pm/config/adom/${adomid}/wanprof/${wanprof}/system/sdwan/service/${service}`
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to create adom policy package
   *
   * @function createAdomPolicyPackage
   * @param {string} adomid - the adom to set the policy in (required)
   * @param {object} policy - the policy to set (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  createAdomPolicyPackage(adomid, policy, callback) {
    const meth = 'adapter-createAdomPolicyPackage';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (policy === undefined || policy === null || policy === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['policy'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'add',
      params: [
        {
          data: policy,
          url: `/pm/pkg/adom/${adomid}`
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to assign adom policy package
   *
   * @function assignAdomPolicyPackage
   * @param {string} adomid - the adom to assign the policy in (required)
   * @param {string} policyid - the policy to be assigned (required)
   * @param {object} assign - the information to assign (required)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  assignAdomPolicyPackage(adomid, policyid, assign, callback) {
    const meth = 'adapter-assignAdomPolicyPackage';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (policyid === undefined || policyid === null || policyid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['policyid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (assign === undefined || assign === null || assign === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['assign'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'add',
      params: [
        {
          data: assign,
          url: `/pm/pkg/adom/${adomid}/${policyid}/scope member`
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }

  /**
   * @summary This is a call to delete adom scripts with filter
   *
   * @function deleteAdomScriptFiltered
   * @param {string} adomid - the adom to delete the script in (required)
   * @param {array} filter - the filter to delete (optional)
   * @param {getCallback} callback - a callback function to return the result
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  deleteAdomScriptFiltered(adomid, filter, callback) {
    const meth = 'adapter-deleteAdomScriptFiltered';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    let localFilter = filter;
    if (adomid === undefined || adomid === null || adomid === '') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'Missing Data', ['adomid'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
    if (localFilter === undefined || localFilter === null || localFilter === '') {
      localFilter = [];
    } else if (typeof localFilter === 'string') {
      localFilter = [localFilter];
    }

    if (this.suspended && this.suspendMode === 'error') {
      const errorObj = this.requestHandlerInst.formatErrorObject(this.id, meth, 'AD.600', [], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    /* HERE IS WHERE YOU VALIDATE DATA */
    const body = {
      id: 1,
      method: 'delete',
      params: [
        {
          filter: localFilter,
          confirm: 1,
          url: `/dvmdb/adom/${adomid}/script`
        }
      ]
    };
    return this.postJsonrpc(body, callback);
  }
}

module.exports = FortiManager;
