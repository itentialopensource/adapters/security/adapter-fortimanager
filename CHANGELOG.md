
## 0.7.4 [10-15-2024]

* Changes made at 2024.10.14_20:55PM

See merge request itentialopensource/adapters/adapter-fortimanager!18

---

## 0.7.3 [08-26-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-fortimanager!16

---

## 0.7.2 [08-14-2024]

* Changes made at 2024.08.14_19:14PM

See merge request itentialopensource/adapters/adapter-fortimanager!15

---

## 0.7.1 [08-07-2024]

* Changes made at 2024.08.06_20:44PM

See merge request itentialopensource/adapters/adapter-fortimanager!14

---

## 0.7.0 [05-16-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-fortimanager!13

---

## 0.6.4 [03-27-2024]

* Changes made at 2024.03.27_13:49PM

See merge request itentialopensource/adapters/security/adapter-fortimanager!12

---

## 0.6.3 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/security/adapter-fortimanager!11

---

## 0.6.2 [03-11-2024]

* Changes made at 2024.03.11_16:19PM

See merge request itentialopensource/adapters/security/adapter-fortimanager!10

---

## 0.6.1 [02-27-2024]

* Changes made at 2024.02.27_11:54AM

See merge request itentialopensource/adapters/security/adapter-fortimanager!9

---

## 0.6.0 [01-05-2024]

* Minor/2023 auto migration

See merge request itentialopensource/adapters/security/adapter-fortimanager!8

---

## 0.5.0 [01-04-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-fortimanager!7

---

## 0.4.0 [05-24-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-fortimanager!6

---

## 0.3.0 [01-21-2022]

- Migration to the latest foundation and broker ready
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/security/adapter-fortimanager!5

---

## 0.2.2 [04-20-2021]

- Patch/adapt 665

See merge request itentialopensource/adapters/security/adapter-fortimanager!4

---

## 0.2.1 [03-05-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/security/adapter-fortimanager!3

---

## 0.2.0 [03-05-2021]

- Add some missing calls - this is not all calls.

See merge request itentialopensource/adapters/security/adapter-fortimanager!2

---

## 0.1.4 [07-07-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/security/adapter-fortimanager!1

---

## 0.1.3 [05-22-2020] & 0.1.2 [05-22-2020] & 0.1.1 [05-22-2020]

- Initial Commit

See commit d613cce

---
